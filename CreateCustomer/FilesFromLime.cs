﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateCustomer
{
    public class FilesFromLime
    {
        public string Company { get; set; }
        public string CustomerNr { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string DeliveryAddress2 { get; set; }
        public string DeliveryAddress3 { get; set; }
        public string ZipCodeCity { get; set; }
        public string Seller { get; set; }
        public string OrgNr { get; set; }
        public string VATnr { get; set; }
        public string Template { get; set; }
    }
}
