﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CreateCustomer
{
    public class Program
    {

        static Logininfo login;
        static List<FilesFromLime> NewCustomer;

        static void Main(string[] args)
        {
            try
            {
                login = new Logininfo();
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Startar" + Environment.NewLine);
                ReadFiles();
                CreateCustomer();
                RemoveOldLogFiles();
                WriteToLog(DateTime.Now.ToString("yyyy-MM-dd H:mm:ss") + " Klar" + Environment.NewLine);
            }
            catch (Exception e)
            {
                Console.Write("Error " + e);
                WriteToLog("Fel " + e.Message);

            }
        }

        public static bool ReadFiles()
        {
            try
            {
                WriteToLog("Startar läsning av filer" + Environment.NewLine);
                NewCustomer = new List<FilesFromLime>();
                List<FilesFromLime> FilesOK = new List<FilesFromLime>();
                List<FilesFromLime> FilesNotOK = new List<FilesFromLime>();

                foreach (string files in Directory.EnumerateFiles(login.FolderFiles))
                {
                    string fileName = Path.GetFileName(files);
                    string destFile = Path.Combine(login.FolderArchive, fileName);
                    string destErrorFile = Path.Combine(login.FolderError, fileName);

                    try
                    {
                        FilesFromLime readData = new FilesFromLime
                        {
                            Company = Tools.Left(fileName, 3),
                        };

                        string line;

                        using (StreamReader file = new StreamReader(files))
                            while ((line = file.ReadLine()) != null)
                            {
                                string[] cells = line.Split(';');

                                readData.CustomerNr = cells[0];
                                readData.Name = cells[1];
                                readData.Address1 = cells[2];
                                readData.Address2 = cells[3];
                                readData.DeliveryAddress2 = cells[4];
                                readData.DeliveryAddress3 = cells[5];
                                readData.ZipCodeCity = cells[6];
                                readData.Seller = cells[7];
                                readData.OrgNr = cells[8];
                                readData.VATnr = cells[9];
                                readData.Template = cells[10];

                                NewCustomer.Add(readData);
                            }
                    }

                    catch (Exception)
                    {
                        WriteToLog("Fel vid läsning av fil " + files + Environment.NewLine);
                        // Flytta filen till Err-mappen
                        File.Move(files, destErrorFile);
                        continue;
                    }
                    File.Move(files, destFile);

                }

                return true;
            }
            catch (Exception e)
            {
                WriteToLog("Fel vid läsning av filer" + Environment.NewLine + e);
                // Flytta filen till Err-mappen
                return false;
            }
        }

        public static void CreateCustomer()
        {
            try
            {
                WriteToLog("Skapar kund ");
                if (NewCustomer.Count > 0)
                {


                    foreach (FilesFromLime Customer in NewCustomer)
                    {
                        WriteToLog("bolag " + Customer.Company + " kundnr " + Customer.CustomerNr);
                        // Token:
                        string Token;
                        switch (Customer.Company)
                        {
                            case "002":
                                Token = "norge";
                                break;

                            case "003":
                                Token = "mafi";
                                break;

                            case "004":
                                Token = "USA";
                                break;

                            case "005":
                                Token = "china";
                                break;
                            default:
                                break;
                        }

                        /*
                        Customer.Nr = kundnumret
                        Customer.Name
                        Customer.Address1
                        Customer.Address2
                        Customer.DeliveryAddress2 = text 1
                        Customer.DeliveryAddress3 = text 2
                        Customer.ZipCodeCity
                        Customer.Seller
                        Customer.OrgNr
                        Customer.VATnr
                        Customer.Template
                        */

                        WriteToLog(Environment.NewLine);
                    }
                }
            }
            catch (Exception e)
            {
                WriteToLog(Environment.NewLine + "Fel vid upplägg ny kund" + Environment.NewLine + e);
            }
        }
        
        private static void WriteToLog(string text)
        {
            try
            {
                Console.Write(text);
                if (login.SkrivLoggFil != null)
                {
                    File.AppendAllText(login.LogFileName, text);
                }
            }
            catch (Exception e)
            {
                Console.Write(e);
                if (login.SkrivLoggFil != null)
                {
                    WriteToLog("Fel uppstod vid skrivning logfil " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
                }
            }
        }
        public static void RemoveOldLogFiles()
        {
            try
            {
                WriteToLog("Raderar logfiler äldre än " + login.MonthSaveLog + " månader" + Environment.NewLine);
                string[] OldFilses = Directory.GetFiles(Path.GetDirectoryName(login.LogFileName));
                foreach (string file in OldFilses)
                {
                    FileInfo fileToDelete = new FileInfo(file);
                    if (fileToDelete.CreationTime < DateTime.Now.AddMonths(0 - login.MonthSaveLog))
                        fileToDelete.Delete();
                }
            }
            catch (Exception e)
            {
                WriteToLog("Fel uppstod vid radering gamla logfiler " + Environment.NewLine + e + Environment.NewLine + e.Message + Environment.NewLine);
            }
        }

    }
}

