﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace CreateCustomer
{
    public class Logininfo
    {
        public string SkrivLoggFil { get; set; }
        public string LogFileName { get; set; }
        public string FolderFiles { get; set; }
        public int MonthSaveLog { get; set; }
        public string FolderArchive { get; set; }
        public string FolderError { get; set; }

        public Logininfo()
        {

            if (ConfigurationManager.AppSettings["SkrivLog"] == "True")
            {
                SkrivLoggFil = ConfigurationManager.AppSettings["SkrivLog"];
            }

            LogFileName = string.Format(@"{0}"
            , Path.GetDirectoryName(ConfigurationManager.AppSettings["FilFörLog"])
            + @"\"
            + Path.GetFileNameWithoutExtension(ConfigurationManager.AppSettings["FilFörLog"])
            + " "
            + DateTime.Now.ToString("yyyy-MM-dd HHmm"))
            + Path.GetExtension(ConfigurationManager.AppSettings["FilFörLog"]);

            MonthSaveLog = int.Parse(ConfigurationManager.AppSettings["AntalMånaderSparaLog"]);

            FolderFiles = string.Format(@"{0}", ConfigurationManager.AppSettings["MappInfiler"]);
            FolderArchive = string.Format(@"{0}", ConfigurationManager.AppSettings["MappArkivera"]);
            FolderError = string.Format(@"{0}", ConfigurationManager.AppSettings["MappError"]);
        }
    }
}
